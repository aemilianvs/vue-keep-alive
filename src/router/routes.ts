import { RouteConfig } from "vue-router";
import List from "../components/List.vue";
import Detail from "../components/Detail.vue";
import DefaultLayout from "../components/DefaultLayout.vue";

const routes: RouteConfig[] = [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      {
        path: "/",
        name: "Root",
        component: List
      },
      {
        path: "/detail",
        name: "Detail",
        component: Detail
      }
    ]
  }
];

export default routes;
