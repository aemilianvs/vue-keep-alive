import { Vue } from "vue-property-decorator";
import VueRouter from "vue-router";
import routes from "./routes";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: "",
  routes
});

export default router;
